%% creating function for rule-based strategy 
% Shyam Jani - 419200
% Anish Pathak - 417770
% Somesh Chaudhari - 419605


function u = controller_rulebased(input)

%------------------------Input parameteres--------------------------------%
w_MGB = input(1);            % get flywheel angular velocity
dw_MGB = input(2);           % get flywheel angular acceleration
T_MGB = input(3);            % get flywheel torque
Q_BT = input(4);

%---------------------Global parameters----------------------------------%
global w_EM_max;             % define maximum motor angular velocity (global) 
global T_EM_max;             % define maximum motor torque (global)
global N_sim;
theta_EM = 0.1;              % define motor inertia
epsilon = 0.01;


if N_sim == 1220
    T_MGB_th = 59.7;               % define torque threshold for Motor mode
    T_MGB_th1 = 31;                % define torque threshold for generator mode 
    Q_BT_max = 36000;              % Upper limit of state of charge 
    Q_BT_min = 6000;               % Lower limit of state of chrage 
    u_LPS_max = 0.3;               % Upper limit of torque split ratio 
    u_LPS_min = -0.601;             %lower limit of torque split ratio
    
    
%------------------------Regeneration------------------------------------%    
    if T_MGB < 0                 
        u = min((interp1(w_EM_max,-T_EM_max,w_MGB)+abs(theta_EM*dw_MGB)+epsilon)/T_MGB,1);
        
%-------------------------Load point shifting motor mode------------------%        
  
    elseif (T_MGB >= T_MGB_th) && (Q_BT>= Q_BT_max)      % load point shifting (cf. Slide 3-8)
        u = min((interp1(w_EM_max,T_EM_max,w_MGB)-abs(theta_EM*dw_MGB)-epsilon)/T_MGB,u_LPS_max);
        
%-------------------------Load point shifting Generator mode--------------%        
    
    elseif ((T_MGB>= T_MGB_th1) && (T_MGB < T_MGB_th) && (Q_BT < Q_BT_max))
        u = max((interp1(w_EM_max,-T_EM_max,w_MGB)+abs(theta_EM*dw_MGB)+epsilon)/T_MGB,u_LPS_min);
        
%-----------------------------Electric Driving----------------------------%        
    
    elseif ((T_MGB>0) && (T_MGB < T_MGB_th1) && (Q_BT > Q_BT_min))
        u=1;
        
%------------------------------Engine only--------------------------------%        
    
    else                         % engine only
        u = 0;
end

%-------------------------------FTP CYCLE--------------------------------%
else
    T_MGB_th = 88.6;            %Torque threshold for motor mode
    T_MGB_th1 = 32;             %Torque threshold for generator mode
    Q_BT_max = 36000;           % upper limit state of charge 
    Q_BT_min = 6000;            % Lower limit state of chrage 
    u_LPS_max = 0.3;            % Higher limit of split ratio
    u_LPS_min = -0.212;         % lower limit of split ratio
 
%----------------------------same conditions as NEDC cycle----------------%
    if T_MGB < 0                 % regeneration (cf. Slide 3-10)
        u = min((interp1(w_EM_max,-T_EM_max,w_MGB)+abs(theta_EM*dw_MGB)+epsilon)/T_MGB,1);
    
    elseif (T_MGB >= T_MGB_th) && (Q_BT>= Q_BT_max)      % load point shifting motor mode
        u = min((interp1(w_EM_max,T_EM_max,w_MGB)-abs(theta_EM*dw_MGB)-epsilon)/T_MGB,u_LPS_max);
    
    elseif ((T_MGB>= T_MGB_th1) && (T_MGB < T_MGB_th) && (Q_BT < Q_BT_max))  % load point shifting generator mode
        u = max((interp1(w_EM_max,-T_EM_max,w_MGB)+abs(theta_EM*dw_MGB)+epsilon)/T_MGB,u_LPS_min);
    
    elseif ((T_MGB>0) && (T_MGB < T_MGB_th1) && (Q_BT > Q_BT_min))      %Electric driving 
        u=1;
        
    else
        u = 0;
    end
end    
end