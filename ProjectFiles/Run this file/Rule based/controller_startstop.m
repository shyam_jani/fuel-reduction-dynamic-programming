%Engine start stop
function state_CE = controller_startstop(input)
w_MGB = input(1);
u = input(2);

if (w_MGB == 0 || u == 1)
    state_CE = 0;
else
    state_CE = 1;
end

   