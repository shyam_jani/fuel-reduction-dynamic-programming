%% Seminar Electromobility--Dynamic Programming (only for FTP)
% Shyam Jani - 419200
% Anish Pathak - 417770
% Somesh Chaudhari - 419605 

%% Dynamic programming for FTP cycle
tic;
%loading driving cycle
load FTP75_cycle_input.mat

%-----------------------Creating grid--------------------------------%
clear grd
grd.Nx{1}    = 101;         % Number of discrete value for SOC
grd.Xn{1}.hi = 0.95;        % SOC upper limit
grd.Xn{1}.lo = 0.15;        % SOC lower limit

grd.Nu{1}    = 4999;        % Number of discrete value for split ratio
grd.Un{1}.hi = 1;           % Torque-Split ratio upper limit
grd.Un{1}.lo = -1;          % Torque-Split ratio lower limit

% -----------------------initial state of battery----------------------%
grd.X0{1} = 0.50;           % SOC init

%--------------------------defining range of SOC of battery---------------------%
grd.XN{1}.hi = 0.5001;      % final SOC range 
grd.XN{1}.lo = 0.50;        % final SOC range

%---------------defining optimization paramters----------------------%
clear prb

prb.W{1} = w_MGB_FTP75';     %Angular velocity of MGB for NEDC
prb.W{2} = T_MGB_FTP75';     %Torque of MGB for NEDC
prb.W{3} = dw_MGB_FTP75';    %Angular acceleration of MGB for NEDC

prb.Ts = 1;                 %Time-step
prb.N  = 1877*1/prb.Ts + 1; %Cycle size

%-----------------------------setting options---------------------------%
options = dpm();
options.MyInf = 1e57;
options.BoundaryMethod = 'Line';    % boundary condition 'Line'.
if strcmp(options.BoundaryMethod,'Line') 
    %these options are only needed if 'Line' is used
    options.Iter = 9;
    options.Tol = 1e-8;
    options.FixedGrid = 0;
end

%-----------------------function call to DPM-----------------------------%
 [res, dyn] = dpm(@hev_qss,[],grd,prb,options);

toc;
run_time = toc - tic;       % total running time of program

%%---------------saving the results for FTP cycle----------------------%
save FTP75_outputs.mat res dyn

%%-----------------------Torque split ratio-------------------------------%

load FTP75_outputs.mat
load FTP75_cycle_input.mat

%----------------------calculating the Torque-Split ratio--------------------------%
u = res.Tm./T_MGB_FTP75';
u(isnan(u)) = 0;            % In case of zero torque, 'u' sets to zero   

%-------------------------------saving the Torque split ratio----------------------%
save split_FTP75_3.mat u


%----------------------creating 1D-lookup table and saving values ----------------------------%
u_in = u;
time = 0:1:1877 ; 
table2 = table(time', u_in');
file_name= 'split_done2.xlsx';
writetable(table2, file_name)
save split_done2.mat u_in time;  
