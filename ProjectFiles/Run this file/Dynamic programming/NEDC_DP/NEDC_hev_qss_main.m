%% Dynamic Programming (NEDC)
% Shyam Jani - 419200
% Anish Pathak - 417770
% Somesh Chaudhari - 419605 

%% Dynamic programming for NEDC(New European Driving Cycle)
tic;
%loading driving cycle
load NEDC_cycle_input.mat

%-----------------Creating grid------------------------------------------%
clear grd
grd.Nx{1}    = 101;         % Number of discrete value for SOC
grd.Xn{1}.hi = 0.95;        % defining SOC upper limit
grd.Xn{1}.lo = 0.15;        % defining SOC lower limit

grd.Nu{1}    = 20001;       % Number of discrete value for split ratio
grd.Un{1}.hi = 1;           % defining Torque Split-ratio upper limit
grd.Un{1}.lo = -1;          % defining Torque Split-ratio lower limit

%----------------------set initial state---------------------------------%
grd.X0{1} = 0.50;           % SOC init

%-----------------defining range of battery for final state------------%
grd.XN{1}.hi = 0.51;        % final SOC range 
grd.XN{1}.lo = 0.501;      % final SOC range

%----------------optimization paramters for------------------------------%
clear prb

prb.W{1} = w_MGB_NEDC';     %Angular velocity of MGB for NEDC
prb.W{2} = T_MGB_NEDC';     %Torque of MGB for NEDC
prb.W{3} = dw_MGB_NEDC';    %Angular acceleration of MGB for NEDC

prb.Ts = 1;                 %Time-step
prb.N  = 1220*1/prb.Ts + 1; %Cycle size

%-----------------------setting options---------------------------------%
options = dpm();
options.MyInf = 1e57;
options.BoundaryMethod = 'Line';    % boundary condition 'Line'.
if strcmp(options.BoundaryMethod,'Line') 
    %these options are only needed if 'Line' is used
    options.Iter = 9;
    options.Tol = 1e-8;
    options.FixedGrid = 0;
end

%------------------------------calling to DPM----------------------------%
[res, dyn] = dpm(@hev_qss,[],grd,prb,options);

toc;
run_time = toc - tic;       % total running time of program

%% saving the results for NEDC cycle
save NEDC_outputs.mat res dyn

%% Torque split ratio


load NEDC_outputs.mat
load NEDC_cycle_input.mat

% calculate the Split-ratio
u = res.Tm./T_MGB_NEDC';
u(isnan(u)) = 0;            % In case of zero torque, 'u' sets to zero   

% save the split ratio
save split_NEDC_3.mat u

%------------------creating 1-D lookup table for DP---------------------%
u_in = u;
time = 0:1:1220; 
table1 = table(time', u_in');
file_name= 'split_done.xlsx';
writetable(table1, file_name)
save split_done.mat u_in time;  %first load split_done file to get simulink result   
