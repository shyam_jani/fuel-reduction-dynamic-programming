This folder contains dynamic programming files for the NEDC.


-->NEDC_cycle_input contains all the input data which is called into NEDC_hev_qss_main.---


-->NEDC_hev_qss_main is the main file from which has the main program and we got our output for the model.--- 


-->NEDC_outputs contains all outpit data which have got from NEDC_hev_qss_main, and controller_startstop_dp is our engine start stop function. 

To run the 'Simulink_model_DPM' please first load the 'split_ratio_NEDC' file in the MATLAB from that Torque split ratio can be called into simulink.!  