%% creating function for engine start stop
% Shyam Jani - 419200
% Anish Pathak - 417770
% Somesh Chaudhari - 419605

function state_CE = controller_startstop_dp(input)

% Defining input parameteres 
T_MGB = input(1);       % Torque of MGB
u     = input(2);       % Split-ratio from the dynamic programming

% defining conditions
if ((T_MGB<0) && (u==1))
    state_CE = 0;
else
   
    state_CE = 1;
end

end